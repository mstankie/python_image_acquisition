init:
	pip install --no-cache-dir -r requirements.txt

stylecheck:
	flake8 image_acquisition/*.py
	flake8 samples/*.py

lint:
	pylint image_acquisition/*.py
	pylint samples/*.py

test:
	pytest --cov=image_acquisition tests/ | tee coverage_report.txt

.PHONY: init stylecheck lint test