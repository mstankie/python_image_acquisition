import setuptools

with open("README.md", "r") as fh:
    README = fh.read()

setuptools.setup(
    name="Image acquisition",
    version="",
    author="Maciej Stankiewicz",
    author_email="maciej@stankiewicz.pro",
    description="Common interface for image streaming devices",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mstankie/python_image_acquisition",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent"
    ],
    python_requires='>=3.6',
)