#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
.. module:: image_acquisition.movie_frames_grabber
    :synopsis: Provides image source extracting frames from movie file.
.. moduleauthor:: Maciej Stankiewicz <maciej@stankiewicz.pro>
"""


import cv2
from .image_source import ImageSource


class MovieFramesGrabber(ImageSource):
    ''' Image source providing access to frames saved in movie file

    This class uses OpenCV VideoCapture to open handle movie files.
    Handled movie types: mp4, mov

    :param file: Movie file path
    :type file: str
    :param loop: If set to yes the movie will be automatically rewind
    :type loop: bool

    :iparam loop: If set to yes the movie will be automatically rewind
    :type loop: bool
    '''

    def __init__(self, file: str, loop: bool = False):
        self._path = file
        self.loop = loop
        self._file = None
        self._frame = None

    def __enter__(self):
        """ (Re)opens movie file """

        self._file = cv2.VideoCapture(self._path)
        self._frame = self._file.get(cv2.CAP_PROP_POS_FRAMES)

        if not self._file.isOpened():
            self._frame = None
            self._file = None
            raise IOError(f"Unable to open {self._path}")

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._file.release()

    def __len__(self) -> int:
        """ returns the number of frames in the movie

        :return: Number of frames in the movie (zero in case of errors)
        :rtype: int
        """

        return int(self._file.get(cv2.CAP_PROP_FRAME_COUNT))

    def _is_at_the_end(self) -> bool:
        """ Checks if the image is at the last frame

        :return: True if current frame is the last one of the movie, False
        :rtype: bool
        """

        # number of captured frames is equal to the total number of frames
        return self._file.get(cv2.CAP_PROP_POS_FRAMES) >= self.__len__()

    def is_active(self) -> bool:
        """Checks whether frames are still available in the file

        If no errors and `loop=True`, shall be always True.

        :return: False if no more frames available; otherwise True
        :rtype: bool
        """

        return self._file is not None and not self._is_at_the_end()

    def grab(self) -> tuple:
        """ Request single image/frame from image source

        :returns: tuple of image (numpy.array) and metadata (dict)
        :rtype: tuple
        """

        if self._file is None:
            return None

        if self._is_at_the_end():
            if self.loop:
                self._frame = 0
                self._file.set(cv2.CAP_PROP_POS_FRAMES, self._frame)
            else:
                self._frame = None
                self._file = None
                return None

        flag, grabbed_frame = self._file.read()
        if flag:
            self._frame = int(self._file.get(cv2.CAP_PROP_POS_FRAMES))
        else:
            # The next frame is not ready, so we try to read it again
            self._frame -= 1
            self._file.set(cv2.CAP_PROP_POS_FRAMES, self._frame)
            return None

        return grabbed_frame, {"frame_no": self._frame-1}
