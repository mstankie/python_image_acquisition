#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
.. module:: image_acquisition
    :synopsis: Image acquisition module for vision system applications
.. moduleauthor:: Maciej Stankiewicz <maciej@stankiewicz.pro>

This module provides common interface for image streaming devices
e.g. cameras, movie files, directories of images

It contains sample file-based devices.
"""

__author__ = "Maciej Stankiewicz"
__license__ = 'GPLv3'
__copyright__ = "Copyright 2020, stankiewicz.pro"

__maintainer__ = "Maciej Stankiewicz"
__email__ = "maciej@stankiewicz.pro"
