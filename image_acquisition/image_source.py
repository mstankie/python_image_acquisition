#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
.. module:: image_acquisition.image_source
    :synopsis: Provides interface class for image source devices.
.. moduleauthor:: Maciej Stankiewicz <maciej@stankiewicz.pro>
"""

from abc import ABC, abstractmethod


class ImageSource(ABC):
    """ Abstract class defining interface of image sources

    Image sources shall be context managing class so that the resource
    will be properly closed, therefore `__enter__` and `__exit__` methods.

    The typical usage::

        with ImageSourceChild as camera:
            frame = camera.grab()
            processFrame(frame)
    """

    @abstractmethod
    def __enter__(self):
        pass

    @abstractmethod
    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    @abstractmethod
    def is_active(self) -> bool:
        """Checks whether the source still produces frames

        :return: False if no more frames available; otherwise True
        :rtype: bool
        """

    @abstractmethod
    def grab(self) -> tuple:
        """ Request single image/frame from image source

        :returns: tuple of image (numpy.array) and metadata (dict)
        :rtype: tuple
        """
