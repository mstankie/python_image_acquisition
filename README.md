Image acquisition package
=========================

This module provides common interface for image streaming devices
e.g. cameras, movie files, directories of images.

It is intended to be used for vision systems applications based on
[OpenCV library](https://opencv.org).

Development of such applications typically requires access to a stream
of pre-acquired images, typically saved as a movie file (AVI)
or collection of image files stored in a directory.

At later stages such application is moved to nect stage when
it receives live images acquired from a camera.

This package provides a common interface for such image streaming
devices, which helps switching between live and off-line streams.

It contains sample avi-file-based device.