#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
.. module:: using_image_acquisition_module
    :synopsis: Sample application to demonstrate package usage.
.. moduleauthor:: Maciej Stankiewicz <maciej@stankiewicz.pro>
"""

import argparse
import sys
import cv2

# Below there are some tricks done so that the sample may import code
# located in the directory above. This requires muting linter and flake.
import os.path  # pylint: disable=wrong-import-order
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
# pylint: disable=wrong-import-position
from image_acquisition.movie_frames_grabber \
    import MovieFramesGrabber  # noqa: E402

if __name__ == "__main__":

    ARG_PARSER = argparse.ArgumentParser("Movie frames grabber test")
    ARG_PARSER.add_argument("movie_file", help="path to movie file (AVI)")
    ARGS = ARG_PARSER.parse_args()

    if not ARGS.movie_file.lower().endswith(".avi"):
        sys.exit("Unsupported file format")

    cv2.namedWindow(ARGS.movie_file)

    with MovieFramesGrabber(ARGS.movie_file) as source:

        while source.is_active():
            IMAGE, _ = source.grab()
            cv2.imshow(ARGS.movie_file, IMAGE)
            if cv2.waitKey(100) == 27:
                break

        cv2.destroyAllWindows()
