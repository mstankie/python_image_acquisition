#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Tests for MovieFramesGrabber class """

import pytest
from image_acquisition.movie_frames_grabber import MovieFramesGrabber
from ._helper import make_test_AVI_movie

import os

FIXTURE_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'files',
    )
if not os.path.exists(FIXTURE_DIR):
    os.mkdir(FIXTURE_DIR)

@pytest.fixture(scope="module",
                params=[
                    {"frame_size": (320, 240), "number_of_frames": 5},
                    {"frame_size": (640, 480), "number_of_frames": 1}
                ],
                ids=[
                    "AVI-320x240-5",
                    "AVI-640x480-1"
                ])
def test_AVI_movie(request):
    """ Create set of test movie files

    This fixture creates set of test AVI movies with specified size and length.
    The files are cleared at the end of the scope.
    """

    FRAME_SIZE = request.param['frame_size']
    NUMBER_OF_FRAMES = request.param['number_of_frames']
    FILE_NAME = f"test_{FRAME_SIZE[0]}x{FRAME_SIZE[1]}-{NUMBER_OF_FRAMES}.avi"
    MOVIE_FILE_PATH = os.path.join(FIXTURE_DIR, FILE_NAME)

    make_test_AVI_movie(MOVIE_FILE_PATH, FRAME_SIZE, NUMBER_OF_FRAMES)

    yield MOVIE_FILE_PATH

    os.remove(MOVIE_FILE_PATH)


def test_instantiate_movie_frames_grabber():
    """ MovieFramesGrabber is not virtual and can be instantiated """
    movie = MovieFramesGrabber("")
    assert isinstance(movie, MovieFramesGrabber)

def test_default_initialises_in_non_loop_mode():
    """ MovieFramesGrabber is not virtual and can be instantiated """
    movie = MovieFramesGrabber("")
    assert not movie.loop

def test_not_opening_movie_files_in_ctor(test_AVI_movie):
    """ MovieFramesGrabber does not open file in constructor """
    movie = MovieFramesGrabber(test_AVI_movie)
    assert movie._path == test_AVI_movie
    assert movie._file is None
    assert not movie.is_active()

def test_open_movie_files(test_AVI_movie):
    """ MovieFramesGrabber opens AVI files """
    with MovieFramesGrabber(test_AVI_movie) as movie:
        assert len(movie) > 0
        assert movie.is_active()

def test_iterates_over_all_frames(test_AVI_movie):
    """ MovieFramesGrabber iterates over all frames in the file """
    with MovieFramesGrabber(test_AVI_movie) as movie:
        frame_no = 0
        while movie.is_active():
            # Pixel values after compression may be slightly changed.
            # The consecutive frames have values changed by 10 starting at 0.
            # Expected value for n-th frame is 10*(n%25)
            # Checking if the value is within +/-5 range from expected.
            frame, metadata = movie.grab()
            # G value after compression is closest, so using [0, 0, 1]
            top_left_pix_value = frame[0, 0, 1]
            expected_value = 10 * (frame_no % 25)
            assert expected_value - 5 < top_left_pix_value < expected_value + 5
            assert metadata["frame_no"] == frame_no
            frame_no += 1
