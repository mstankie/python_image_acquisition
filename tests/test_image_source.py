#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Tests for ImageSource class """

import pytest
from image_acquisition.image_source import ImageSource


def test_instantiate_image_source_class():
    """ ImageSource is an interface and so cannot be instantiated """
    with pytest.raises(TypeError) as error:
        # pylint: disable=abstract-class-instantiated
        _ = ImageSource()
    assert "abstract class" in str(error.value)
