#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" helper functions for image_acquisition module tests """

import cv2
import numpy

def make_test_AVI_movie(movie_path,
                        frame_size=(320, 240),
                        number_of_frames=5,
                        fps=20.0):
    """ Generate test movie

    This function generates test AVI movie files with specified size and length.
    Every frame is filled with single colour for each all RGB values are set to
    its index. After reaching 255th frame couour is reset to 0 and continues.
    """

    assert movie_path[-4:].upper() == ".AVI"

    # As width/height order differs in OpenCV and numpy -assembling explicitely
    WIDTH, HEIGHT = frame_size
    LAYERS = 3 # VideoWriter.write expects BGR images
    FRAME_IMAGE_SIZE = HEIGHT, WIDTH, LAYERS
    MOVIE_SIZE = WIDTH, HEIGHT

    video_encoder = cv2.VideoWriter_fourcc(*"MPEG")
    video_writer = cv2.VideoWriter(movie_path, video_encoder, fps, MOVIE_SIZE)

    for frame_no in range(number_of_frames):
        frame = numpy.ones(FRAME_IMAGE_SIZE, numpy.uint8) * (frame_no % 25)
        video_writer.write(frame * 10) # avoid slight changes to compresion

    video_writer.release()
